#!/bin/bash

git clone https://aur.archlinux.org/yay.git
cd yay/
makepkg -si

yay -S auto-cpufreq
sudo systemctl enable --now auto-cpufreq

sudo pacman -S xorg sddm plasma-desktop dolphin terminator plasma-nm ark grub-customizer
yay -S brave-bin vscodium-bin

sudo systemctl enable sddm
/bin/echo -e "\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m"
sleep 5
reboot
