#!/bin/bash

ln -sf /usr/share/zoneinfo/Asia/Kathmandu /etc/localtime
hwclock --systohc --utc
sed -i '177s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "nepux" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 nepux.localdomain nepux" >> /etc/hosts
echo root:password | chpasswd

pacman -S grub efibootmgr networkmanager network-manager-applet dialog wpa_supplicant broadcom-wl-dkms reflector base-devel linux-headers xdg-user-dirs xdg-utils alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack bash-completion ufw sof-firmware os-prober xf86-video-intel intel-ucode

mkdir /boot/efi
mount /dev/sda3 /boot/efi
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable fstrim.timer
systemctl enable ufw

useradd -m peace
echo peace:password | chpasswd

echo "peace ALL=(ALL) ALL" >> /etc/sudoers.d/peace


printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"




